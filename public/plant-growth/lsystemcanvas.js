const canvas = document.getElementById("lsystemCanvas");
const canvasContext = canvas.getContext("2d");

const canvasFontSize = 16;
canvasContext.font = canvasFontSize.toString() + "px jetBrainsMono";
const lineLength = 25;
canvasContext.lineWidth = 5;
canvasContext.lineCap = "round";

const iterations = 3;
const angleOffset = 20 * Math.PI / 180;

const moveChar = "F";
const backtrackChar = "]";
const backtrackEndChar = "[";
const charsAngleOffsets = {"+": angleOffset, "-": -angleOffset};

const currentCharPos = {
  x: canvas.width - (canvasFontSize * 4),
  y: canvas.height - (canvasFontSize)
}
const turtleStartPosition = {
  x: canvas.width / 2,
  y: canvas.height - (canvasFontSize)
}
const turtleShellRadius = 15;


let lSystemString = "X";
let lSystemStringi = 0;
let iteration = 1;
let history = [];
let turtle = {};



function drawRules() {
  canvasContext.fillStyle = canvasColors.white;

  canvasContext.fillText(
    "X -> F[+X]F[-X]+X",
    (canvasFontSize - 1) * 2.5,
    canvasFontSize
  );
  canvasContext.fillText(
    "F -> FF",
    (canvasFontSize - 1) * 2.5,
    canvasFontSize * 2 + 2
  );
  canvasContext.fillText(
    "X    20°    3",
    (canvasFontSize - 1) * 2.5,
    canvasFontSize * 3 + 2
  );
//   canvasContext.fillText(
//     "20°",
//     (canvasFontSize - 1) * 2.5,
//     canvasFontSize * 4 + 2
//   );
}

function drawHistory(backtracking) {
  canvasContext.fillStyle = canvasColors.white;

  let backtrackingMini = -1
  if (backtracking) {
    backtrackingMini = history.findLastIndex((e) => e == "[" );
  }
  
  history.forEach((char, i) => {
    if (backtracking && i == backtrackingMini) {
      canvasContext.fillStyle = canvasColors.red;
    }
    canvasContext.fillText(char, 0, canvas.height - ((canvasFontSize + 2) * i));
  });
}

function drawCurrentChar(char, backtracking) {
  if (charsAngleOffsets[char]) {
    let angleDir = Math.sign(charsAngleOffsets[char]);
    if (backtracking) {
      angleDir *= -1;
    }

    let startAngle = (Math.PI / 2) + Math.PI;
    let endAngle = startAngle + (angleOffset * angleDir);
    
    let arcx = currentCharPos.x * 1.1;
    if (angleDir == 1) {
      arcx = currentCharPos.x * 1.125;
    }
    
    canvasContext.beginPath();
    canvasContext.arc(
      arcx, currentCharPos.y,
      canvas.width - currentCharPos.x,
      Math.min(startAngle, endAngle), Math.max(startAngle, endAngle)
    );
    
    if (backtracking) {
      canvasContext.strokeStyle = canvasColors.red;
    } else {
      canvasContext.strokeStyle = canvasColors.white;
    }
    canvasContext.stroke();
  }

  if (char == "X" || char == "[") {
    canvasContext.fillStyle = canvasColors.whiteTransparent;
  } else if (backtracking) {
    canvasContext.fillStyle = canvasColors.red;
  } else {
    canvasContext.fillStyle = canvasColors.white;
  }

  canvasContext.font = (canvas.width - currentCharPos.x).toString() + "px jetBrainsMono";
  canvasContext.fillText(char, currentCharPos.x, currentCharPos.y);
  canvasContext.font = canvasFontSize.toString() + "px jetBrainsMono";
}

function drawPath() {
  canvasContext.strokeStyle = canvasColors.white;
  canvasContext.stroke(turtle.p);
}

function drawTurtle() {
  canvasContext.shadowColor = canvasColors.green;
  canvasContext.shadowBlur = 3;
  
  //legs
  let legsPath = new Path2D;
  [-1, 1].forEach(dir => {
    [45, 135].forEach(angle => {
      const legAngle = turtle.r + (dir * (angle * Math.PI / 180));

      legsPath.moveTo(turtle.x, turtle.y);
      legsPath.lineTo(
        turtle.x + ((turtleShellRadius * 1.3) * Math.sin(legAngle)),
        turtle.y - ((turtleShellRadius * 1.3) * Math.cos(legAngle))
      )
    });
  });
  canvasContext.strokeStyle = canvasColors.green;
  canvasContext.lineWidth *= 1.5;
  canvasContext.stroke(legsPath);
  
  // tail
  let tailPath = new Path2D;
  tailPath.moveTo(turtle.x, turtle.y);
  tailPath.lineTo(
    turtle.x - ((turtleShellRadius + 2) * Math.sin(turtle.r)),
    turtle.y + ((turtleShellRadius + 2) * Math.cos(turtle.r))
  );
  canvasContext.stroke(tailPath);
  canvasContext.lineWidth /= 1.5;

  // head
  let headPath = new Path2D;
  headPath.moveTo(turtle.x, turtle.y);
  headPath.lineTo(
    turtle.x + ((turtleShellRadius * 1.5) * Math.sin(turtle.r)),
    turtle.y - ((turtleShellRadius * 1.5) * Math.cos(turtle.r))
  );
  canvasContext.lineWidth *= 3;
  canvasContext.strokeStyle = canvasColors.green;
  canvasContext.stroke(headPath);  
  canvasContext.lineWidth /= 3;

  // shell
  canvasContext.beginPath();
  canvasContext.arc(turtle.x, turtle.y, turtleShellRadius, 0, Math.PI * 2);
  canvasContext.fillStyle = canvasColors.greenAlt;
  canvasContext.fill();
  canvasContext.lineWidth /= 2;
  canvasContext.strokeStyle = canvasColors.green;
  canvasContext.stroke();
  canvasContext.lineWidth *= 2;

  canvasContext.shadowBlur = 0;
}

function drawLSystem(char, backtracking) {
  canvasContext.clearRect(0, 0, canvas.width, canvas.height);

  drawRules();
  drawHistory(backtracking);
  drawCurrentChar(char, backtracking);
  drawPath();
  drawTurtle();
}


function stepLSystem(lSystemString, backtracking=false) {
  let timeoutIteration = 1;
//   if (iteration <= 2) {
//     timeoutIteration = 0;
//   }

  let char = null;
  let dir = 1;
  if (backtracking) {
    char = history.pop();
    dir = -1;
  } else {
    char = lSystemString.substring(lSystemStringi, lSystemStringi + 1);
    history.push(char);
  }

  if (char == moveChar) {
    turtle.x += lineLength * Math.sin(turtle.r) * dir;
    turtle.y -= lineLength * Math.cos(turtle.r) * dir;

    if (backtracking) {
      turtle.p.moveTo(turtle.x, turtle.y);
    } else {
      turtle.p.lineTo(turtle.x, turtle.y);
    }
  } else if (charsAngleOffsets[char]) {
    turtle.r += charsAngleOffsets[char] * dir;
  } else if (char == backtrackChar) {
    history.pop();
  }

  drawLSystem(char, char == backtrackChar || (backtracking && char != backtrackEndChar));

  setTimeout(() => {
    if (!backtracking) {
      lSystemStringi += 1;
    }
    
    if (lSystemStringi < lSystemString.length) {
      stepLSystem(lSystemString, char == backtrackChar || (backtracking && char != backtrackEndChar));
    } else {
      iterateLSystem();
    }
  }, canvasSleepTime * timeoutIteration);
}

function iterateLSystem() {
  if (iteration > iterations) {
    iteration = 1
    lSystemString = "X"
  }
  
  iteration += 1;
  
  lSystemString = lSystemString.replace(/F/g, "FF");
  lSystemString = lSystemString.replace(/X/g, "F[+X]F[-X]+X");

  lSystemStringi = 0;
  history = [];
  turtle = {
    x: turtleStartPosition.x,
    y: turtleStartPosition.y,
    r: 0,
    p: new Path2D()
  };
  turtle.p.moveTo(turtle.x, turtle.y);

  stepLSystem(lSystemString);
}

iterateLSystem();
