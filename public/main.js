const jetBrainsMono = new FontFace("jetBrainsMono", "url('JetBrainsMono.ttf')");
document.fonts.add(jetBrainsMono);

const canvasSleepTime = 1000;
const canvasColors = {
  white: "white",
  black: "black",
  whiteTransparent: "rgba(255, 255, 255, 0.5)",
  whiteMoreTransparent: "rgba(255, 255, 255, 0.2)",
  whiteVeryTransparent: "rgba(255, 255, 255, 0.1)",
  blackTransparent: "rgba(0, 0, 0, 0.5)",
  blackVeryTransparent: "rgba(0, 0, 0, 0.15",
  green: "rgb(100, 150, 100)",
  greenAlt: "rgb(31, 46, 31)",
  greenTransparent: "rgba(100, 150, 100, 0.5)",
  greenVeryTransparent: "rgba(100, 150, 100, 0.1)",
  red: "rgb(170, 70, 70)",
  redTransparent: "rgba(170, 70, 70, 0.25)"
}

const green = "#00b100";


const textWaveSpeed = 1000;
let totalLength = 0;

document.querySelectorAll(".anime-link").forEach((target, i) => {
  const newHTML = target.innerHTML.split("").map((letter, i) => {
    totalLength += 1;

    return "<div class='anime-link-text' id='anime-link" + totalLength.toString() + "'>" + letter + "</div>";
  }).join("");
  
  target.innerHTML = newHTML;
});

for (let i = 1; i <= totalLength; i++) {
  const animeTargets = document.querySelectorAll("#anime-link" + i.toString());

  setTimeout(() => {
    anime({
      targets: animeTargets,

      translateY: -10,
      color: ["#ffffff", green],
      transform: ["scale(1)", "scale(1.4)"],
      
      direction: "alternate",
      easing: "easeInExpo",
      loop: true,

      duration: textWaveSpeed,
      delay: textWaveSpeed * 2.5
    });
  }, (textWaveSpeed * (i / totalLength)));
};
