const shadowCanvas = document.getElementById("shadowcastingCanvas");
const shadowCanvasContext = shadowCanvas.getContext("2d");

shadowCanvasContext.lineWidth = 2;

let noise2D = makeNoise2D(Date.now());
const frequency = 0.1;
const wallThreshold = 0.5;

const sColumns = 9;
const sCenterCol = Math.floor(sColumns / 2);
const sCellSize = Math.floor(shadowCanvas.width / sColumns);
const sCenterPosition = (sColumns * sCellSize) / 2;

const sSlopeDash = [4, 4];
const sTileDash = [(sCellSize - 4) / 3, (sCellSize - 4) / 3];
const sMarkerSize = 5;
const sTileSpacing = 2;

const cardinalDirs = [
  {x: 0, y: 1},
  {x: 0, y: -1},
  {x: 1, y: 0},
  {x: -1, y: 0}
];

let diri = cardinalDirs.length - 1;
let sMap = {};


function drawShadows(row, dir) {
  shadowCanvasContext.clearRect(0, 0, shadowCanvas.width, shadowCanvas.height);

  shadowCanvasContext.fillStyle = canvasColors.whiteVeryTransparent;
  shadowCanvasContext.fillRect(0, 0, sColumns * sCellSize, sColumns * sCellSize);

  drawShadowRow(row);

  for (const [_tileStr, tile] of Object.entries(sMap)) {
    drawShadowTile(tile);
  }

  if (row.tilei > 0) {
    const currentTile = row.tiles[row.tilei - 1].trueTile;
    let color = canvasColors.green;
    let markerSize = sMarkerSize;

    if (!currentTile.visible) {
      color = canvasColors.red;
      markerSize *= 2;
    }
    shadowCanvasContext.fillStyle = color;

    shadowCanvasContext.fillRect(
      (currentTile.x * sCellSize) + (sCellSize / 2) - markerSize,
      (currentTile.y * sCellSize) + (sCellSize / 2) - markerSize,
      (markerSize * 2),
      (markerSize * 2)
    );
  }
  
  drawShadowSlopes(row, dir);
}

function drawShadowRow(row) {
  const rowStartTile = row.tiles[0].trueTile;
  const rowEndTile = row.tiles[row.tiles.length - 1].trueTile;

  shadowCanvasContext.fillStyle = canvasColors.greenTransparent;
  shadowCanvasContext.fillRect(
    rowStartTile.x * sCellSize,
    rowStartTile.y * sCellSize,
    ((rowEndTile.x + 1) - rowStartTile.x) * sCellSize,
    ((rowEndTile.y + 1) - rowStartTile.y) * sCellSize
  )
}

function drawShadowTile(tile) {
  shadowCanvasContext.setLineDash(sTileDash);
  let color = canvasColors.white;

  if (tile.wall) {
    color = canvasColors.black;
  } else {
    color = canvasColors.white;
  }

  shadowCanvasContext.strokeStyle = color;
  shadowCanvasContext.fillStyle = color;

  if (tile.visible) {
    shadowCanvasContext.fillRect(
      (tile.x * sCellSize) + sTileSpacing,
      (tile.y * sCellSize) + sTileSpacing,
      sCellSize - (sTileSpacing * 2),
      sCellSize - (sTileSpacing * 2)
    )
  } else {
    shadowCanvasContext.strokeRect(
      (tile.x * sCellSize) + sTileSpacing,
      (tile.y * sCellSize) + sTileSpacing,
      sCellSize - (sTileSpacing * 2),
      sCellSize - (sTileSpacing * 2)
    );

    // if (tile.wall) {
    //   shadowCanvasContext.fillStyle = canvasColors.white;
    // } else {
    //   shadowCanvasContext.fillStyle = canvasColors.black;
    // }
    // shadowCanvasContext.fillRect(
    //   (tile.x * sCellSize) + (sCellSize / 2) - 2,
    //   (tile.y * sCellSize) + (sCellSize / 2) - 2,
    //   4,
    //   4
    // );
  }
}

function drawShadowSlopes(row, dir) {
  const startSlopePoint = {
    x: sCenterPosition + (sCenterPosition * row.startSlope),
    y: sCenterPosition + (sCenterPosition * Math.sign(dir.y))
  };
  const endSlopePoint = {
    x: sCenterPosition + (sCenterPosition * row.endSlope),
    y: startSlopePoint.y
  };

  if (dir.y == 0) {
    startSlopePoint.x = sCenterPosition + (sCenterPosition * Math.sign(dir.x));
    startSlopePoint.y = sCenterPosition + (sCenterPosition * row.startSlope);

    endSlopePoint.x = startSlopePoint.x;
    endSlopePoint.y = sCenterPosition + (sCenterPosition * row.endSlope);
  }

  const slopePath = new Path2D;

  slopePath.moveTo(sCenterPosition, sCenterPosition);
  slopePath.lineTo(startSlopePoint.x, startSlopePoint.y);
  slopePath.moveTo(sCenterPosition, sCenterPosition);
  slopePath.lineTo(endSlopePoint.x, endSlopePoint.y);
  
  shadowCanvasContext.strokeStyle = canvasColors.red;
  shadowCanvasContext.setLineDash(sSlopeDash);
  shadowCanvasContext.lineWidth = 4;  
  shadowCanvasContext.stroke(slopePath);
  shadowCanvasContext.lineWidth = 2;
}


function tileName(tile) {
  return tile.x.toString() + ", " + tile.y.toString();
}

function generateMap() {
  noise2D = makeNoise2D(Date.now());
  sMap = {};
  
  for (let x = 0; x < sColumns; x++) {
    for (let y = 0; y < sColumns; y++) {
      let value = 1;

      if (y == sCenterCol + 1 || ((y < sCenterCol && x == sCenterCol -1) || (y > sCenterCol && x == sCenterCol + 1))) {
        value = 0;
      } else if (x >= sCenterCol - 1 && x <= sCenterCol + 1 && y >= sCenterCol - 1 && y <= sCenterCol + 1) {
        value = 0;
      } else {
        value = (noise2D(
          ((x * sCellSize) + (sCellSize / 2)) * frequency,
          ((y * sCellSize) + (sCellSize / 2)) * frequency
        ) + 1) / 2;
      }

      const tile = {
        x: x, y: y,
        wall: value > wallThreshold,
        visible: x == sCenterCol && y == sCenterCol
      };

      sMap[tileName(tile)] = tile;
    }
  }
}


function nextRow(row, dir) {
  let newRow = {
    depth: row.depth + 1,
    startSlope: row.startSlope,
    endSlope: row.endSlope,
    tiles: [],
    tilei: 0,
    previousWall: null,
    nextable: false
  };

  const minCol = Math.floor((newRow.depth * newRow.startSlope) + 0.5);
  const maxCol = Math.ceil((newRow.depth * newRow.endSlope) - 0.5);

  for (let col = minCol; col <= maxCol; col++) {
    const tile = {x: col, y: newRow.depth};

    let tileRotated = {x: tile.x, y: tile.y};
    if (dir.x == 0) {
      tileRotated.y *= Math.sign(dir.y);
    } else {
      tileRotated.x = tile.y * Math.sign(dir.x);
      tileRotated.y = tile.x;
    }

    tile.trueTile = sMap[tileName({
      x: sCenterCol + tileRotated.x,
      y: sCenterCol + tileRotated.y
    })];
  
    newRow.tiles.push(tile);
  }

  return newRow;
}

function updateVisibility(row, tile) {
  const col = tile.x;
  const minCol = row.depth * row.startSlope;
  const maxCol = row.depth * row.endSlope;

  if ((col >= minCol && col <= maxCol) || tile.trueTile.wall) {
    tile.trueTile.visible = true;
  }
}

function tileSlope(tile) {
  const rowDepth = tile.y;
  const col = tile.x;
  const slope = (2 * col - 1) / (2 * rowDepth);

  return slope;
}


function shadowcastDir() {
  if (diri == cardinalDirs.length - 1) {
    generateMap();
  }

  diri = (diri + 1) % cardinalDirs.length;
  
  
  const dir = cardinalDirs[diri];
  const rows = [];
  rows.push(nextRow({depth: 0, startSlope: -1, endSlope: 1}, dir));
  
  shadowcastRow();
  
  function shadowcastRow() {
    if (rows.length == 0) {
      shadowcastDir();
      return;
    }

    let row = rows.pop();
    shadowcastTile();

    function updateSlopes(tileOffset) {
      if (row.previousWall == true && !tileOffset.trueTile.wall) {
        row.startSlope = tileSlope(tileOffset);
  
      } else if (row.previousWall == false && tileOffset.trueTile.wall && row.depth < sCenterCol) {
        const slopedRow = {depth: row.depth, startSlope: row.startSlope, endSlope: tileSlope(tileOffset)};
        const newRow = nextRow(slopedRow, dir);
  
        return newRow;
      }

      return null;
    }

    function shadowcastTile() {
      if (row.tilei == row.tiles.length) {
        shadowcastRow();
        return;
      }

      const tileOffset = row.tiles[row.tilei];
      row.tilei += 1;
  
      updateVisibility(row, tileOffset);
      const newRow = updateSlopes(tileOffset);

      row.previousWall = tileOffset.trueTile.wall;

      if (row.tilei == row.tiles.length && row.previousWall == false && row.depth < sCenterCol) {
        rows.push(nextRow(row, dir));
      }

      drawShadows(row, dir);

      if (newRow) {
        rows.push(row);
        row = newRow;
      }
      
      setTimeout(shadowcastTile, canvasSleepTime);
    }
  }
}
shadowcastDir();
